public static class Constants
{
	public static readonly short SCALE = 20;
	public static readonly int WINDOW_WIDTH_PRESET = 800;
	public static readonly int WINDOW_HEIGHT_PRESET = 800;
	public static readonly int WINDOW_WIDTH = WINDOW_WIDTH_PRESET / SCALE;
	public static readonly int WINDOW_HEIGHT = WINDOW_HEIGHT_PRESET / SCALE;
	
	public static readonly int GAME_REFRESH_RATE_MS = 500;
	public static readonly int START_SNAKE_LENGTH = 5;
	
	// Even though i spend hours on this i have failed to center the game
	// The snake always spawned in the left down corner and not in the middle
	public static readonly int BORDER_LEFT = -17;
	public static readonly int BORDER_RIGHT = 26;
	public static readonly int BORDER_UP = -7;
	public static readonly int BORDER_DOWN = 31;
	
}


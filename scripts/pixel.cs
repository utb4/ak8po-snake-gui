using System;

public class Pixel
{
	public int XPos { get; set; }
	public int YPos { get; set; }
	public string texturePath { get; set; }
}

using System.Collections.Generic;
using System;
using Godot;

public class Snake
{
	public Pixel Head { get; private set; }
	public List<Pixel> Body { get; private set; }
	public Direction Movement { get; set; }

	public Snake(Pixel head)
	{
		Head = head;
		Body = new List<Pixel>();
		Movement = Direction.Right;
		InitializeBody();
	}
	
	 private void InitializeBody()
	{
		// Initialize the body with 5 parts to the left of the head
		for (int i = 1; i <= Constants.START_SNAKE_LENGTH; i++)
		{
			Body.Add(new Pixel
			{
				XPos = Head.XPos - i,
				YPos = Head.YPos,
				texturePath = "res://graphics/snakeBody.png"
			});
		}
	}
	
	// Moves snake. If collision occurs, ends returns false
	public bool Move(int score)
	{
		Pixel newHeadPixel = new Pixel
		{
			XPos = Head.XPos,
			YPos = Head.YPos,
			texturePath = "res://graphics/snakeHead.png"
		};
		switch (Movement)
		{
			case Direction.Up: newHeadPixel.YPos--; break;
			case Direction.Down: newHeadPixel.YPos++; break;
			case Direction.Left: newHeadPixel.XPos--; break;
			case Direction.Right: newHeadPixel.XPos++; break;
		}
		// First check for collisions before adding the current head to the body
		bool collision = IsCollision();
		if (collision) return true;

		Body.Insert(0, new Pixel { XPos = Head.XPos, YPos = Head.YPos, texturePath = "res://graphics/snakeBody.png" });
		Head = newHeadPixel;

		if ((score + Constants.START_SNAKE_LENGTH) < Body.Count)
		{
			Body.RemoveAt(Body.Count - 1);
		}
		return false;
}

	// Returns true if snake colides with its body or borders
	private bool IsCollision()
	{
		if (Head.XPos <= Constants.BORDER_LEFT || Head.XPos >= Constants.BORDER_RIGHT ||
			   Head.YPos <= Constants.BORDER_UP || Head.YPos >= Constants.BORDER_DOWN)
		{
			return true;
		}
		
		foreach (Pixel body in Body)
		{
			if (Head.XPos == body.XPos && Head.YPos == body.YPos)
				return true;
		}
		return false;
	}
}

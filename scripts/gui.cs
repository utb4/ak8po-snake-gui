using Godot;
using System;
using System.Collections.Generic;

public partial class gui : Node
{
	private MarginContainer _container;
	private int screenWidth;
	private int screenHeight;

	public gui(MarginContainer container)
	{
		_container = container;
	}

	public void AddPixel(Pixel pixel)
	{
		var sprite = new Sprite2D();
		var texture = (Texture2D)GD.Load(pixel.texturePath);
		sprite.Texture = texture;
		sprite.Scale = new Vector2(Constants.SCALE, Constants.SCALE);
		sprite.Position = new Vector2(pixel.XPos * Constants.SCALE, pixel.YPos * Constants.SCALE);
		sprite.ZIndex = 5; // Draw pixel in foreground above the HUD
		_container.AddChild(sprite);
	}
	
	public void DrawSnakeBody(IEnumerable<Pixel> snakeBody)
{
	foreach (var pixel in snakeBody)
	{
		AddPixel(pixel);
	}
}
	public void Clear(){
		   while (_container.GetChildCount() > 0)
	{
		Node child = _container.GetChild(0);
		_container.RemoveChild(child);
		child.QueueFree();
	}}
	
	public void DisplayGameOverMessage(int score, Label label)
	{
		Clear();
		label.Visible=true;
		label.Text = "Game score: " + (score);
	}
}


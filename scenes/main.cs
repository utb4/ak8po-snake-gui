using Godot;
using System;
using System.Diagnostics;

public partial class main : Node2D
{
	// Score should start with 0
	int score = 0;
	private Label scoreLabel;
	// Random generator for berry placing
	Random randomGenerator = new();
	bool gameover = false;
	
	private Pixel berry;
	private Snake snake;
	private gui g;	
	private Stopwatch stopWatch;	
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		scoreLabel = GetNode<Label>("HUD/ScoreLabel");
		scoreLabel.Text = "SCORE: " + score.ToString(); 
		
		MarginContainer container = GetNode<MarginContainer>("MarginContainer");
		g = new gui(container);	
		berry = new Pixel
		{
			XPos = randomGenerator.Next(Constants.BORDER_LEFT + 2, Constants.BORDER_RIGHT - 2),
			YPos = randomGenerator.Next(Constants.BORDER_UP + 2, Constants.BORDER_DOWN - 2),
			texturePath = "res://graphics/berry.png"
		};
		
		snake = new Snake (new Pixel
		{
			XPos = (Constants.BORDER_LEFT + Constants.BORDER_RIGHT) / 2 ,
			YPos = (Constants.BORDER_UP + Constants.BORDER_DOWN) / 2 ,
			texturePath = "res://graphics/snakeHead.png"
		});
		
		g.AddPixel(snake.Head);		
		g.AddPixel(berry);
		stopWatch = Stopwatch.StartNew();
	}
	

	public override void _Process(double delta)
	{
		if (stopWatch.ElapsedMilliseconds >= Constants.GAME_REFRESH_RATE_MS)
		{	
			g.Clear();
	   		g.AddPixel(snake.Head);
			g.DrawSnakeBody(snake.Body);
			g.AddPixel(berry);
			
			if (snake.Move(score))
			{
				g.DisplayGameOverMessage(score, GetNode<Label>("GameOverLabel"));
			}

			CheckBerryCollision();
			stopWatch.Restart();
		}
	}
	
	private void CheckBerryCollision()
	{
		if (berry.XPos == snake.Head.XPos && berry.YPos == snake.Head.YPos)
		{
			score++;
			scoreLabel.Text = "SCORE: " + score.ToString();
			berry.XPos = randomGenerator.Next(1, Constants.WINDOW_WIDTH - 2);
			berry.YPos = randomGenerator.Next(1, Constants.WINDOW_HEIGHT - 2);
		}
	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (@event is InputEventKey eventKey && eventKey.Pressed)
		{
			if (eventKey.Keycode == Key.Up && snake.Movement != Direction.Down)
			{
				snake.Movement = Direction.Up;
			}
			if (eventKey.Keycode == Key.Down && snake.Movement != Direction.Up)
			{
				snake.Movement = Direction.Down;
			}
			if (eventKey.Keycode == Key.Left && snake.Movement != Direction.Right)
			{
				snake.Movement = Direction.Left;
			}
			if (eventKey.Keycode == Key.Right && snake.Movement != Direction.Left)
			{
				snake.Movement = Direction.Right;
			}									
		}
	}
}
